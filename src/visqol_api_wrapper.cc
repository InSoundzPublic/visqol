#ifndef _VISQOL_API_WRAPPER_
#include "visqol_api.h"

namespace Visqol{
extern "C" {
		VisqolApi* visqol_init(int sample_rate,char* model_file,int use_speech){
			VisqolConfig config;
			config.mutable_audio()->set_sample_rate(sample_rate);
			config.mutable_options()->set_allow_unsupported_sample_rates(false);
			config.mutable_options()->set_svr_model_path(model_file);
			config.mutable_options()->set_use_speech_scoring(use_speech);
			config.mutable_options()->set_use_unscaled_speech_mos_mapping(false);
			VisqolApi* visqol = new VisqolApi();
			auto status = visqol->Create(config);

			if (!status.ok()) {
				std::cout<<status.ToString()<<std::endl;
				return NULL;
			}
			
			return visqol;
		}

		void visqol_free(VisqolApi* visqol){
			delete(visqol);	
		}

		SimilarityResultMsg* visqol_measure(VisqolApi* visqol, double *reference, size_t ref_len, 
				double* degraded, size_t deg_len,
				double *fvnsim_size,
				double *nsim,double* center_freq_bands)
		{
			auto ref_span = absl::Span<double>(reference,ref_len);
			auto deg_span = absl::Span<double>(degraded,deg_len);
			auto result = visqol->Measure(ref_span, deg_span);
			if (!result.ok()) {
				return NULL;
			}
			SimilarityResultMsg* sim_result = new SimilarityResultMsg();
			*sim_result = result.ValueOrDie();
			return sim_result;
		}

		double visqol_get_mos(SimilarityResultMsg* sim_result){
			return sim_result->moslqo();
		}

		int visqol_get_fvnsim_size(SimilarityResultMsg* sim_result){
			return sim_result->fvnsim_size();
		}


		void visqol_get_fvnsim(SimilarityResultMsg* sim_result,
				double *fvnsim, 
				double *center_freq_bands){
			for (int i = 0; i < sim_result->fvnsim_size(); i++) {
				fvnsim[i] = sim_result->fvnsim(i);
				center_freq_bands[i] = sim_result->center_freq_bands(i);
			}
		}
	}
}
#endif
