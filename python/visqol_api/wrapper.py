import numpy as np
from ctypes import *
import os

PACKAGE_PATH = os.path.dirname(__file__)

MODEL_FILE=PACKAGE_PATH+"/libsvm_nu_svr_model.txt"

lib = CDLL(PACKAGE_PATH+"/libvisqol.so")

visqol_init = lib.visqol_init
visqol_init.restype = c_void_p
visqol_init.argtypes = [c_int, c_char_p,c_int]

visqol_free = lib.visqol_free
visqol_free.argtypes = [c_void_p]

visqol_measure = lib.visqol_measure
visqol_measure.restype = c_void_p
visqol_measure.argtypes = [c_void_p,
        np.ctypeslib.ndpointer(dtype=np.float64, flags='C'),c_int,
        np.ctypeslib.ndpointer(dtype=np.float64, flags='C'),c_int]

visqol_get_mos = lib.visqol_get_mos
visqol_get_mos.restype = c_double
visqol_get_mos.argtypes = [c_void_p]

visqol_get_fvnsim_size = lib.visqol_get_fvnsim_size
visqol_get_fvnsim_size.restype = c_int
visqol_get_fvnsim_size.argtypes = [c_void_p]

visqol_get_fvnsim = lib.visqol_get_fvnsim
visqol_get_fvnsim.argtypes = [c_void_p,
        np.ctypeslib.ndpointer(dtype=np.float64, flags='C'),
        np.ctypeslib.ndpointer(dtype=np.float64, flags='C')]


class Visqol():
    def __init__(self,sample_rate,use_speech):
        model_file_c = c_char_p(MODEL_FILE.encode('utf-8'))
        self._visqol = visqol_init(c_int(sample_rate),model_file_c,c_int(use_speech))

    def __del__(self):
        visqol_free(self._visqol)

    def measure(self,ref,deg):
        msg = visqol_measure(self._visqol, 
                np.ascontiguousarray(ref,dtype=np.float64),len(ref),
                np.ascontiguousarray(deg,dtype=np.float64),len(deg))

        mos = visqol_get_mos(msg)
        fvnsim_size = visqol_get_fvnsim_size(msg)
        fvnsim = np.zeros(shape=(fvnsim_size))
        center_freq_bands = np.zeros(shape=(fvnsim_size))
        visqol_get_fvnsim(msg,fvnsim, center_freq_bands)
        return mos, fvnsim, center_freq_bands
