from setuptools import setup
from setuptools import Extension
from setuptools import find_packages
from setuptools.dist import Distribution

setup(
        name='visqol_api',
        version='0.0.1',
        author='Emil Winebrand',
        packages=['visqol_api'],
        include_package_data=True,
        zip_safe=False,
)

