#!/bin/bash

## Clean up
rm -f results.csv
rm -f ref_file.wav
rm -f test_file.wav

FREQ=16000

## Male
REF_FILE=Male_Female/Male/NAR_English_Male_48Khz.wav
sox $REF_FILE -r $FREQ -c 1 -b 16 ref_file.wav 
for TEST_FILE in $(find ./Male_Female/Male/ -name \*.wav); do
	echo $TEST_FILE
	sox $TEST_FILE -r $FREQ -c 1 -b 16 test_file.wav 
	MOS=$(./bazel-bin/visqol --use_speech_mode --reference_file ref_file.wav --degraded_file test_file.wav | awk 'NR>2{print $2}')
	echo "MOS: $MOS"
	echo "$REF_FILE, $TEST_FILE, $MOS" >>results.csv
done


## Female
REF_FILE=./Male_Female/Female/NAR_English_Female_48Khz.wav
sox $REF_FILE -r $FREQ -c 1 -b 16 ref_file.wav 
for TEST_FILE in $(find ./Male_Female/Female/ -name \*.wav); do
	echo $TEST_FILE
	sox $TEST_FILE -r $FREQ -c 1 -b 16 test_file.wav 
	MOS=$(./bazel-bin/visqol --use_speech_mode --reference_file ref_file.wav --degraded_file test_file.wav | awk 'NR>2{print $2}')
	echo "MOS: $MOS"
	echo "$REF_FILE, $TEST_FILE, $MOS" >>results.csv
done
